# First stage: Installer
FROM node:16-alpine as installer
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .

# EXPOSE 80
# CMD ["npm", "run", "dev",]

# Second stage: Captain
FROM node:16-alpine as captain
WORKDIR /usr/src/app
COPY package*.json ./
COPY --from=installer /usr/src/app/node_modules ./node_modules
COPY . .
RUN npm run build

# Third stage: Deployer
FROM node:16-alpine as deployer
WORKDIR /usr/src/app
COPY --from=captain /usr/src/app/build ./build
EXPOSE 80
CMD ["node", "build/server.js"]
