export class apiResponse {
    private status: number | null
    private message: string | null
    private data: any | null
    private error: any | null
    constructor(status: number, message: string, data: any, error: any) {
        this.status = status
        this.message = message
        this.data = data
        this.error = error
    }
}

export const adminDashboard = {
    location_range: 0,
    store_timing: 1,
    advertisement_time: 2
}
export const getArea = (current: { lat: any, long: any }, RadiusInKm: number) => {
    const differenceForLat = RadiusInKm / 111.12
    const curve = Math.abs(Math.cos((2 * Math.PI * parseFloat(current.lat)) / 360.0))
    const differenceForLong = RadiusInKm / (curve * 111.12)
    const minLat = parseFloat(current.lat) - differenceForLat
    const maxLat = parseFloat(current.lat) + differenceForLat
    const minlon = parseFloat(current.long) - differenceForLong
    const maxlon = parseFloat(current.long) + differenceForLong;
    return {
        min: {
            lat: minLat,
            long: minlon,
        },
        max: {
            lat: maxLat,
            long: maxlon,
        },
    };
}

export const inWords = (num) => {
    var a = ['', 'One ', 'Two ', 'Three ', 'Four ', 'Five ', 'Six ', 'Seven ', 'Eight ', 'Nine ', 'Ten ', 'Eleven ', 'Twelve ', 'Thirteen ', 'Fourteen ', 'Fifteen ', 'Sixteen ', 'Seventeen ', 'Eighteen ', 'Nineteen '];
    var b = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
    if ((num = num.toString()).length > 9) return 'overflow';
    let n: any = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'Crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'Lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'Thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'Hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) : '';
    str += ' Only'
    return str;
}

export const cachingTimeOut = 1800

export const GST_percentage = 18

export const not_first_one = (a1: Array<any>, a2: Array<any>) => {
    var a = [], diff = [];
    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }
    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        }
    }
    for (var k in a) {
        diff.push(k);
    }
    return diff;
}

export const loginStatus = {
    regular: 0,
    google: 1,
    facebook: 2,
    twitter: 3,
}

export const URL_decode = (url) => {
    let folder_name = [], image_name
    url.split("/").map((value, index, arr) => {
        image_name = url.split("/")[url.split("/").length - 1]
        folder_name = (url.split("/"))
        folder_name.splice(url.split("/").length - 1, 1)
    })
    return [folder_name.join('/'), image_name]
}
export const userStatus = {
    user: 0,
    admin: 1,
    store_owner: 2,
    support: 3,
    staff: 4,
    upload: 5,
}

export const userAccountType = {
    personal: 0,
    office: 1,
}

export const loginType = {
    custom: 0,
    google: 1,
    facebook: 2,
    apple: 3,
}

export const SMS_message = {
    OTP_verification: `V Pristine by PVR app verification code:`,
    OTP_forgot_password: `V Pristine by PVR app forgot password verification code:`,
    new_appointment: (high_light: any, booking_no: any, dateTime: any) => { return `Your order ${booking_no}, service ${high_light} has been booked successfully on ${dateTime} V Pristine by PVR.` },
    new_appointment_for_store: (high_light: any, booking_no: any, dateTime: any) => { return `You have received a new appointment ${booking_no}, service ${high_light} has been booked successfully on ${dateTime} V Pristine by PVR.` },
    new_appointment_for_admin: (high_light: any, dateTime: any, amount: any, city: any) => { return `Hello V Pristine admin, You have received a new appointment, service ${high_light} has been booked successfully on ${dateTime} V Pristine in city ${city} and amount is ${amount} by PVR.` },
    extend_appointment: (high_light: any, booking_no: any, dateTime: any) => { return `Your order ${booking_no}, service ${high_light} has been extended on ${dateTime} V Pristine by PVR Admin.` },
    expert_contact: (high_light: any, user_name: any, phoneNumber: any, dateTime: any) => { return `Hello V Pristine admin, You have received expert contact request, on this service ${high_light} on that date ${dateTime} from customer ${user_name}. Contact no: ${phoneNumber} ` },
    expert_contact_user: () => { return `Your expert contact request has been sent to the V Pristine PVR Admin.` },
}

export const appointmentStatus = {
    pending: 0,
    approve: 1,
    running: 2,
    completed: 3,
    cancel: 4,
}

export const appointmentStatusArray = [0, 1, 2, 3, 4]

export const paymentMethodStatus = {
    online: 0,
    cash: 1,
}

export const addressType = {
    home: 0,
    office: 1,
    store: 2,
    other: 3,
}

export const documentType = {
    aadharCard: 0,
    panCard: 1,
    addressProof: 2,
    driverLicense: 3
}

export const gender = {
    male: 0,
    female: 1,
    other: 2
}

export const appointment_assign_status = {
    assign: 0,
    active: 1,
    reached: 2,
    start_service: 3,
    complete_service: 4,
}

export const couponType = {
    flatDiscount: 0,
    voucherDiscount: 1,
}

export const days_name_array = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

export const appointment_assign_status_array = ['assign', 'active', 'reached', 'start_service', 'complete_service']

export const notification_types = {
    new_appointment: async (data: any) => {
        return {
            template: {
                title: `New appointment ${data?.booking_no} schedule`, body: `Your ${data?.service_high_light} service has been scheduled on ${data?.dateTime}`
            },
            data: {
                type: 1, appointmentId: data?.appointmentId, message: `New appointment schedule`, click_action: "FLUTTER_NOTIFICATION_CLICK",
            }
        }
    },
    new_appointment_storeOwner: async (data: any) => {
        return {
            template: {
                title: `New appointment ${data?.booking_no}`, body: `${data?.name} has been requested to ${data?.service_high_light} service on ${data?.dateTime}`
            },
            data: {
                type: 1, appointmentId: data?.appointmentId, message: `New appointment schedule`, click_action: "FLUTTER_NOTIFICATION_CLICK",
            }
        }
    },
    approve_appointment: async (data: any) => {
        return {
            template: {
                title: `Appointment approved`, body: `Your ${data?.service_high_light} service appointment has been approved.`
            },
            data: {
                type: 1, appointmentId: data?.appointmentId, message: `Appointment approved`, click_action: "FLUTTER_NOTIFICATION_CLICK",
            }
        }
    },
    cancel_appointment: async (data: any) => {
        return {
            template: {
                title: `Appointment cancel`, body: `Your ${data?.service_high_light} service appointment has been canceled.`
            },
            data: {
                type: 1, appointmentId: data?.appointmentId, message: `Appointment canceled`, click_action: "FLUTTER_NOTIFICATION_CLICK",
            }
        }
    },
    assign_appointment: async (data: any) => {
        return {
            template: {
                title: `New appointment assign`, body: `You've to assign ${data?.service_high_light} service by store owner.`
            },
            data: {
                type: 1, appointmentId: data?.appointmentId, message: `Appointment assign`, click_action: "FLUTTER_NOTIFICATION_CLICK",
            }
        }
    },
    extend_appointment: async (data: any) => {
        return {
            template: {
                title: `Appointment extended`, body: `Your ${data?.service_high_light} service appointment has been extended.`
            },
            data: {
                type: 1, appointmentId: data?.appointmentId, message: `Appointment extended`, click_action: "FLUTTER_NOTIFICATION_CLICK",
            }
        }
    },
    user_to_owner_callback: async (data: any) => {
        return {
            template: {
                title: `New callback enquiry arrived`, body: `${data?.name} sent new enquiry service at ${new Date(data?.dateTime).toLocaleString().split(',')[0]} ${new Date(data?.dateTime).toLocaleString().split(',')[1]}`
            },
            data: {
                callbackId: data?.callbackId, message: `New callback enquiry`, click_action: "FLUTTER_NOTIFICATION_CLICK",
            }
        }
    },
    staff_got_out_for_service: async (data: any) => {
        return {
            template: {
                title: `Staff got out for Appointment`, body: `Our staff has got out for the your appointment ${data.booking_no} service .`
            },
            data: {
                type: 1, appointmentId: data?.appointmentId, message: `Our staff has got out for the your appointment service`, click_action: "FLUTTER_NOTIFICATION_CLICK",
            }
        }
    },
    staff_reached: async (data: any) => {
        return {
            template: {
                title: `Staff reached`, body: `Your appointment ${data.booking_no} service started in few minutes.`
            },
            data: {
                type: 1, appointmentId: data?.appointmentId, message: `Staff reached at your address`, click_action: "FLUTTER_NOTIFICATION_CLICK",
            }
        }
    },
    service_start_by_staff: async (data: any) => {
        return {
            template: {
                title: `Service started`, body: `Your appointment ${data.booking_no} service started.`
            },
            data: {
                type: 1, appointmentId: data?.appointmentId, message: `Service started`, click_action: "FLUTTER_NOTIFICATION_CLICK",
            }
        }
    },
    service_complete: async (data: any) => {
        return {
            template: {
                title: `Service completed`, body: `Your appointment service has been completed.`
            },
            data: {
                type: 1, appointmentId: data?.appointmentId, message: `Service completed`, click_action: "FLUTTER_NOTIFICATION_CLICK",
            }
        }
    },
    service_complete_by_staff: async (data: any) => {
        return {
            template: {
                title: `Service completed`, body: `You've successfully appointment completed.`
            },
            data: {
                type: 1, appointmentId: data?.appointmentId, message: `Service completed`, click_action: "FLUTTER_NOTIFICATION_CLICK",
            }
        }
    },
    expert_contact_to_admin: async (data: any) => {
        return {
            template: {
                title: `New expert contact request`, body: `${data?.name} has been requested to ${data?.service_high_light} service on ${data?.dateTime}`
            },
            data: {
                type: 1, message: `New expert contact details`, click_action: "FLUTTER_NOTIFICATION_CLICK",
            }
        }
    },
}

export const distance_calculation = async (lat1, lat2, lon1, lon2) => {
    lon1 = lon1 * Math.PI / 180;
    lon2 = lon2 * Math.PI / 180;
    lat1 = lat1 * Math.PI / 180;
    lat2 = lat2 * Math.PI / 180;

    // Haversine formula
    let dlon = lon2 - lon1;
    let dlat = lat2 - lat1;
    let a = Math.pow(Math.sin(dlat / 2), 2)
        + Math.cos(lat1) * Math.cos(lat2)
        * Math.pow(Math.sin(dlon / 2), 2);

    let c = 2 * Math.asin(Math.sqrt(a));

    // Radius of earth in kilometers. Use 3956
    // for miles
    let r = 6371;

    // calculate the result
    return (c * r);
}

export const image_folder = ['category', 'sub_category', 'store_image', 'profile_image', 'store_icon', 'advertisement', 'service', 'feedback', 'proof', 'location_image', 'invoice', 'expert_contact', 'appointment', 'partner', 'coupon']

// Will remove all falsy values: undefined, null, 0, false, NaN and "" (empty string)
export const cleanArray = (actual) => {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i]);
        }
    }
    return newArray;
}

export const GetSortOrder = (prop) => {
    return function (a, b) {
        if (a[prop] > b[prop]) {
            return 1;
        } else if (a[prop] < b[prop]) {
            return -1;
        }
        return 0;
    }
}

export const booking_no_generation = (count: any, serial_number: any) => {
    for (let i = count.toString().length; i < 6; i++) {
        serial_number += 0
    }
    return serial_number += count.toString()
}

export const invoice_no_generation = (count: any, serial_number: any) => {
    serial_number += 'IN'
    for (let i = count.toString().length; i < 4; i++) {
        serial_number += 0
    }
    return serial_number += count.toString()
}

export const location_area_status = {
    "west": "West",
    "north": "North",
    "south": "South",
    "east": "East"
}

export const mail_filename = {
    new_store_owner: "new_store_owner.html",
    new_staff_user: "new_staff_user.html",
    new_appointment: "new_appointment.html",
    invoice: "invoice.html",
}

export const mail_params_value = {
    new_store_owner: ["name", "email", "phoneNumber", "alterPhoneNumber", "store_name", "store_city", "store_address", "store_state", "store_PINcode", "token", "reset_url"],
    new_staff_user: ["name", "email", "phoneNumber", "alterPhoneNumber", "store_name", "store_city", "store_address", "store_state", "store_PINcode"],
    new_appointment: ["name", "email", "phoneNumber", "service_high_light", "dateTime", "appointmentId", "booking_no", "store_name", "store_city", "store_address", "store_state", "store_PINcode"]
}

export const monthName = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', "JUN", 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']

export const tax = {
    gst_original: async (amount: any) => await Math.round(((amount * 100 / (100 + GST_percentage)) + Number.EPSILON) * 100) / 100,
    gst_half_amount: async (amount: any) => await Math.round(((amount * GST_percentage / (100 + GST_percentage) / 2) + Number.EPSILON) * 100) / 100,
    gst_full_amount: async (amount: any) => await Math.round((((amount * GST_percentage) / (100 + GST_percentage)) + Number.EPSILON) * 100) / 100,
}

export const URL = {
    initiateTransaction: (mid, orderId) => { return `/theia/api/v1/initiateTransaction?mid=${mid}&orderId=${orderId}` },
    transactionStatus: `/v3/order/status`,
}