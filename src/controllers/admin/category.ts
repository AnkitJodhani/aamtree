"use strict"
import { logger, reqInfo } from '../../helpers/winston_logger'
import { categoryModel } from '../../database'
import { apiResponse, URL_decode } from '../../common'
import { Request, Response } from 'express'
import mongoose from 'mongoose'
import { deleteImage } from '../../helpers/S3'
import async from 'async'
import { responseMessage } from '../../helpers/response'

const ObjectId = mongoose.Types.ObjectId

export const add_category = async (req: Request, res: Response) => {
    reqInfo(req)
    let body = req.body,
        search = new RegExp(`^${body.name}$`, "ig")
    let user: any = req.header('user')
    body.createdBy = user._id
    try {
        let isExist = await categoryModel.findOne({ name: { $regex: search }, isActive: true })
        if (isExist) {
            return res.status(409).json(new apiResponse(409, responseMessage?.dataAlreadyExist('Category'), {}, {}));
        }
        let response = await new categoryModel(body).save()
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.addDataSuccess('Category'), {}, {}));
        else return res.status(409).json(new apiResponse(409, responseMessage?.addDataError, {}, {}));
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const update_category = async (req: Request, res: Response) => {
    reqInfo(req)
    let body = req.body,
        id = body?.id,
        search = new RegExp(`^${body.name}$`, "ig")
    let user: any = req.header('user')
    body.createdBy = user._id
    try {
        let isExist = await categoryModel.findOne({ name: { $regex: search }, _id: { $ne: ObjectId(id) }, isActive: true })
        if (isExist) {
            return res.status(409).json(new apiResponse(409, responseMessage?.dataAlreadyExist('Category'), {}, {}));
        }
        let response = await categoryModel.findOneAndUpdate({ _id: ObjectId(id), isActive: true }, body)
        if (body?.image != response?.image) {
            let [folder_name, image_name] = await URL_decode(response?.image)
            await deleteImage(image_name, folder_name)
        }
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.updateDataSuccess('Category'), {}, {}));
        else return res.status(409).json(new apiResponse(409, responseMessage?.updateDataError('Janki'), {}, {}));

    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const delete_category = async (req: Request, res: Response) => {
    reqInfo(req)
    let id = req.params.id
    try {
        let response = await categoryModel.findOneAndUpdate({ _id: ObjectId(id), isActive: true }, { isActive: false })
        if (response?.image) {
            let [folder_name, image_name] = await URL_decode(response?.image)
            await deleteImage(image_name, folder_name)
        }
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.deleteDataSuccess('Category'), {}, {}));

    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const by_id_category = async (req: Request, res: Response) => {
    reqInfo(req)
    let { id } = req.params
    try {
        let response = await categoryModel.findOne({ _id: ObjectId(id), isActive: true })
        return res.status(200).json(new apiResponse(200, responseMessage?.getDataSuccess('category'), response, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const get_category_pagination = async (req: Request, res: Response) => {
    reqInfo(req)
    let { page, limit } = req.body, match: any = {}, skip = 0
    try {

        let [category_data, category_count] = await async.parallel([
            (callback) => {
                categoryModel.aggregate([
                    { $match: { isActive: true } },
                    { $sort: { createdAt: -1 } },
                    { $skip: skip },
                    { $limit: limit },
                ]).then(data => { callback(null, data) }).catch(err => { console.log(err) })
            },
            (callback) => { categoryModel.countDocuments({ isActive: true }).then(data => { callback(null, data) }).catch(err => { console.log(err) }) },
        ])
        return res.status(200).json(new apiResponse(200, responseMessage.getDataSuccess('category'), {
            category_data: category_data,
            state: {
                page: page,
                limit: limit,
                page_limit: Math.ceil(category_count / (limit) as number)
            }
        }, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}




