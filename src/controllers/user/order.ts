"use strict"
import { logger, reqInfo } from '../../helpers/winston_logger'
import { orderModel } from '../../database'
import { apiResponse, URL_decode } from '../../common'
import { Request, Response } from 'express'
import mongoose, { Aggregate } from 'mongoose'
import { deleteImage } from '../../helpers/S3'
import async from 'async'
import { responseMessage } from '../../helpers/response'
import { statSync } from 'fs'

const ObjectId = mongoose.Types.ObjectId

export const add_order = async (req: Request, res: Response) => {
    reqInfo(req)
    let body = req.body
    let user: any = req.header('user')
    body.createdBy = ObjectId(user._id)
    try {
        let isExist = await orderModel.findOne({ cartId: { $in: body?.cartId }, isActive: true })
        if (isExist) {
            return res.status(409).json(new apiResponse(409, responseMessage?.dataAlreadyExist('order'), {}, {}));
        }
        body.cartId = body?.cartId.map(data => { return ObjectId(data) })
        let response = await new orderModel(body).save()
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.addDataSuccess('order'), {}, {}));
        else return res.status(409).json(new apiResponse(409, responseMessage?.addDataError, {}, {}));
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const update_order = async (req: Request, res: Response) => {
    reqInfo(req)
    let body = req.body,
        id = body?.id
    let user: any = req.header('user')
    body.updatedBy = user._id
    try {
        let isExist = await orderModel.findOne({ cartId: ObjectId(body?.cartId), isActive: true })
        if (isExist) {
            return res.status(409).json(new apiResponse(409, responseMessage?.dataAlreadyExist('order'), {}, {}));
        }
        let response = await orderModel.findOneAndUpdate({ _id: ObjectId(id), isActive: true }, body)
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.updateDataSuccess('order'), {}, {}));
        else return res.status(409).json(new apiResponse(409, responseMessage?.updateDataError(''), {}, {}));
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const delete_order = async (req: Request, res: Response) => {
    reqInfo(req)
    let id = req.params.id
    try {
        let response = await orderModel.findOneAndUpdate({ _id: ObjectId(id), isActive: true }, { isActive: false })

        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.deleteDataSuccess('order'), {}, {}));

    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const cancel_order = async (req: Request, res: Response) => {
    reqInfo(req)
    let id = req.params.id
    try {
        let response = await orderModel.findOneAndUpdate({ _id: ObjectId(id), isActive: true }, { status: 2 })

        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.updateDataSuccess('cancel order'), {}, {}));

    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}

export const by_id_order = async (req: Request, res: Response) => {
    reqInfo(req)
    let { id } = req.params
    try {
        let response = await orderModel.findOne({ _id: ObjectId(id), isActive: true })
        if (response)
            return res.status(200).json(new apiResponse(200, responseMessage?.getDataSuccess('order'), response, {}))
        else
            return res.status(404).json(new apiResponse(404, responseMessage?.getDataNotFound('order'), {}, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const get_order_pagination = async (req: Request, res: Response) => {
    reqInfo(req)
    let { page, limit } = req.body, match: any = {}, skip = 0
    try {
        match.isActive = true
        let [order_data, order_count] = await async.parallel([
            (callback) => {
                orderModel.aggregate([
                    { $match: match },
                    { $sort: { createdAt: -1 } },
                    { $skip: skip },
                    { $limit: limit },
                ]).then(data => { callback(null, data) }).catch(err => { console.log(err) })
            },
            (callback) => { orderModel.countDocuments({ isActive: true }).then(data => { callback(null, data) }).catch(err => { console.log(err) }) },
        ])
        return res.status(200).json(new apiResponse(200, responseMessage.getDataSuccess('product'), {
            order_data: order_data,
            state: {
                page: page,
                limit: limit,
                page_limit: Math.ceil(order_count / (limit) as number)
            }
        }, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const order_by_user = async (req: Request, res: Response) => {
    reqInfo(req)
    let id = req.params.id
    try {
        let response = await orderModel.aggregate([
            {
                $lookup: {
                    from: "orders",
                    localField: "_id",
                    foreignField: "userId",
                    as: "order"
                }
            },
            {
                $project: {
                    _id: 1,
                    name: 1,
                    "order._id": 1,
                    "cart_name": 1,

                }
            }
        ])
        return res.status(200).json(new apiResponse(200, responseMessage?.getDataSuccess('order '), response, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const get_order_status = async (req: Request, res: Response) => {
    reqInfo(req)
    let { status, page, orderType, limit } = req.body, match: any = {}, user: any = req.header('user'), response: any, skip: any = {}, order_count: any = {}
    try {

        match = { isActive: true, isBlock: false, createdBy: ObjectId(user?._id) }
        console.log(user);

        if (status != null && status != undefined)
            match.status = status
        if (orderType != null && orderType != undefined)
            match.orderType = orderType;

        [response, order_count] = await Promise.all([
            await orderModel.aggregate([
                { $match: match },
                {
                    $lookup: {
                        from: "carts",
                        let: { cartId: '$cartId', },
                        pipeline: [{
                            $match: {
                                $expr: {
                                    $and: [
                                        { $in: ['$_id', '$$cartId'] },
                                        { $eq: ['$isActive', true] },
                                    ],
                                },
                            },
                        }, {
                            $project: { productId: 1, quantity: 1, price: 1 }
                        }
                        ],
                        as: "carts"
                    },
                },

                { $sort: { createdAt: -1 } },
                { $limit: limit },
            ]),
            await orderModel.countDocuments(match)
        ])
        return res.status(200).json(new apiResponse(200, responseMessage?.getDataSuccess('cart by order'), {
            order_data: response,
            state: {
                page,
                limit,
                page_limit: Math.ceil(order_count / (limit) as number)
            }
        }, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}

