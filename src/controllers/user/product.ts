

import { reqInfo } from '../../helpers/winston_logger'
import { productModel, shopModel } from '../../database'
import { apiResponse, not_first_one, URL_decode } from '../../common'
import { Request, Response } from 'express'
import mongoose from 'mongoose'
import async from 'async'
import { deleteImage } from '../../helpers/S3'
import { responseMessage } from '../../helpers/response'
const ObjectId = mongoose.Types.ObjectId

export const get_product_pagination = async (req: Request, res: Response) => {
    reqInfo(req)
    let { page, limit, search } = req.body, match: any = {}, user: any = req.header('user'), response: any, product_count: any = {}, search_match: any = {}
    try {
        if (search) {
            var titleArray: Array<any> = []
            var nameArray: Array<any> = []
            var productTitleArray: Array<any> = []
            search = search.split(" ")
            search.forEach(data => {
                titleArray.push({ "title": { $regex: data, $options: 'si' } })
                nameArray.push({ "category.name": { $regex: data, $options: 'si' } })
            })
            match['$or'] = [{ $and: titleArray }, { $and: nameArray }]
        }

        [response, product_count] = await Promise.all([
            await productModel.aggregate([
                { $sort: { createdAt: -1 } },
                {
                    $lookup: {
                        from: "categories",
                        let: { categoryId: '$categoryId', },
                        pipeline: [{
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ['$_id', '$$categoryId'] },
                                        { $eq: ['$isActive', true] },
                                    ],
                                },
                            },
                        },
                        ],
                        as: "category"
                    },
                },
                { $match: match },
                { $skip: ((((page) as number - 1) * (limit) as number)) },
                { $limit: limit },
            ]),
            await productModel.aggregate([
                {
                    $lookup: {
                        from: "categories",
                        let: { categoryId: '$categoryId', },
                        pipeline: [{
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ['$_id', '$$categoryId'] },
                                        { $eq: ['$isActive', true] },
                                    ],
                                },
                            },
                        },
                        { $project: { _id: 1 } },
                        ],
                        as: "category"
                    },
                },
                { $match: match },
                { $count: "count" }
            ])
        ])


        return res.status(200).json(new apiResponse(200, responseMessage?.getDataSuccess('search by product '), {
            order_data: response,
            state: {
                page,
                limit,
                page_limit: Math.ceil((product_count[0]?.count || 1) / (limit) as number)
            }
        }, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}

