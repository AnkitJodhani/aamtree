"use strict"
import { logger, reqInfo } from '../../helpers/winston_logger'
import { shopModel, userModel } from '../../database'
import { apiResponse, URL_decode, getArea } from '../../common'
import { Request, response, Response } from 'express'
import mongoose, { ObjectId } from 'mongoose'
import { deleteImage } from '../../helpers/S3'
import async from 'async'
import { responseMessage } from '../../helpers/response'
import { match } from 'assert'

const ObjectId = mongoose.Types.ObjectId

export const get_shop = async (req: Request, res: Response) => {
    reqInfo(req)
    let { page, limit, latitude, longitude } = req.body, match: any = {}, shop_data: any, shop_count: any
    try {
        let location_data = await getArea({ lat: latitude, long: longitude }, 10)
        console.log('====================================');
        console.log(location_data);
        console.log('====================================');
        let isInRange = await shopModel.aggregate([
            {
                $addFields: {
                    latitude: { $first: "$location.coordinates" },
                    longitude: { $last: "$location.coordinates" },
                }
            },
            {
                $match: {
                    latitude: { $gte: location_data.min.lat, $lte: location_data.max.lat },
                    longitude: { $gte: location_data.min.long, $lte: location_data.max.long },
                    isActive: true,
                }
            },

        ])
        console.log(isInRange)
        if (isInRange.length == 0) return res.status(200).json(new apiResponse(200, `Sorry! We don't serve in your area yet.`, {}, {}))

        const [shop_data, shop_count] = await Promise.all([
            await shopModel.aggregate([
                { $match: match },
                { $sort: { createdAt: -1 } },
                { $skip: ((((page) as number - 1) * (limit) as number)) },
                { $limit: (limit) as number },
                {
                    $project: {
                        _id: 1,
                        Subtitle: 1,
                        title: 1,
                        location: 1,
                        Pincode: 1,
                        orderType: 1,
                        Price: 1,
                        Rating: 1,
                        SubRating: 1,
                        Address: 1,
                        image: 1,
                    }
                }
            ]),
            await shopModel.countDocuments()
        ])
        return res.status(200).json(new apiResponse(200, responseMessage.getDataSuccess('shop'), {
            shop_data: shop_data,
            state: {
                page: page,
                limit: limit,
                page_limit: Math.ceil(shop_count / (limit) as number),

            }
        }, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
// export const add_order_shop = async (req: Request, res: Response) => {
//     reqInfo(req)
//     let { id, body } = req.body,

//         user: any = req.header('user'), match: any = {}
//     try {

//         let userData = await shopModel.findOne({ _id: ObjectId(id), isActive: true, isBlock: false })

//         if (userData?.orderType == 0 && userData?.orderType == 1) {
//             return res.status(200).json(new apiResponse(200, responseMessage?.addDataSuccess('shop'), {}, {}))
//         }
//         else {
//             return res.status(200).json(new apiResponse(200, responseMessage?.addDataError, {}, {}))
//         }
//         let response: any = await new shopModel(body).save()
//         return res.status(200).json(new apiResponse(200, responseMessage?.addDataSuccess('shop'), response, {}))

//     } catch (error) {
//         console.log(error)
//         return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
//     }
// }


