"use strict"
import { logger, reqInfo } from '../../helpers/winston_logger'
import { orderModel, trackOrderModel } from '../../database'
import { apiResponse, URL_decode } from '../../common'
import { Request, response, Response } from 'express'
import mongoose from 'mongoose'
import { deleteImage } from '../../helpers/S3'
import async from 'async'
import { responseMessage } from '../../helpers/response'

const ObjectId = mongoose.Types.ObjectId

export const add_tracker_order = async (req: Request, res: Response) => {
    reqInfo(req)
    let body = req.body
    let user: any = req.header('user')
    body.createdBy = user._id
    try {
        let response = await new trackOrderModel(body).save()
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.addDataSuccess('tracker_order'), {}, {}));
        else return res.status(409).json(new apiResponse(409, responseMessage?.addDataError, {}, {}));
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const order_Submitted = async (req: Request, res: Response) => {
    reqInfo(req)
    let body = req.body,
        id = req.body?.id
    let user: any = req.header('user')
    body.createdBy = user._id
    try {

        let response = await new orderModel.findOne({ _id: ObjectId(id), isActive: true })

        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.addDataSuccess('tracker_order'), {}, {}));
        else return res.status(409).json(new apiResponse(409, responseMessage?.addDataError, {}, {}));
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
