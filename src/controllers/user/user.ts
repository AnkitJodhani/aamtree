"use strict"
import { reqInfo } from '../../helpers/winston_logger'
import { userModel } from '../../database'
import { loginStatus, apiResponse, URL_decode, SMS_message, userStatus } from '../../common'
import bcryptjs from 'bcryptjs'
import jwt from 'jsonwebtoken'
import config from 'config'
import { Request, Response } from 'express'
import { sendSMS } from '../../helpers/aws_sns'
import axios from 'axios'
import { forgot_password_mail, email_verification_mail } from '../../helpers/aws_mail'
import { deleteImage } from '../../helpers/S3'
import { responseMessage } from '../../helpers/response'
const refresh_jwt_token_secret = process.env.REFRESH_JWT_TOKEN_SECRET

const ObjectId = require('mongoose').Types.ObjectId
const jwt_token_secret = config.get('jwt_token_secret')

export const signUp = async (req: Request, res: Response) => {
    reqInfo(req)

    try {
        let body = req.body,
            otpFlag = 1, // OTP has already assign or not for cross-verification
            authToken = 0
        let isAlready: any = await userModel.findOne({ phoneNumber: body?.phoneNumber, isActive: true, userType: userStatus.user })
        if (isAlready) return res.status(409).json(new apiResponse(409, responseMessage?.alreadyPhoneNumber, {}, {}))
        if (isAlready?.isBlock == true) return res.status(403).json(new apiResponse(403, responseMessage?.accountBlock, {}, {}))
        while (otpFlag == 1) {
            for (let flag = 0; flag < 1;) {
                authToken = await Math.round(Math.random() * 1000000)
                if (authToken.toString().length == 6) {
                    flag++
                }
            }
            let isAlreadyAssign = await userModel.findOne({ otp: authToken })
            if (isAlreadyAssign?.otp != authToken) otpFlag = 0
        }
        body.authToken = authToken
        body.otp = authToken
        body.otpExpireTime = new Date(new Date().setMinutes(new Date().getMinutes() + 10))
        await new userModel(body).save().then(async data => {
            await sendSMS(data?.countryCode, data?.phoneNumber, `${SMS_message.OTP_verification} ${authToken}`)
        })
        return res.status(200).json(new apiResponse(200, responseMessage?.signupSuccess, {}, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}

export const login = async (req: Request, res: Response) => {
    let body = req.body,
        otpFlag = 1, // OTP has already assign or not for cross-verification
        otp = 0,
        response,
        data
    try {
        while (otpFlag == 1) {
            for (let flag = 0; flag < 1;) {
                otp = await Math.round(Math.random() * 1000000)
                if (otp.toString().length == 6) {
                    flag++
                }
            }
            let isAlreadyAssign = await userModel.findOne({ otp: otp })
            if (isAlreadyAssign?.otp != otp) otpFlag = 0
        }
        if (body?.phoneNumber) {
            data = await userModel.findOne({ phoneNumber: body?.phoneNumber, isActive: true, userType: userStatus.user })
            if (!data) return res.status(400).json(new apiResponse(400, responseMessage?.invalidPhoneNumber, {}, {}))
            if (data.isBlock == true) return res.status(403).json(new apiResponse(403, responseMessage?.accountBlock, {}, {}))
            await sendSMS(data?.countryCode, data?.phoneNumber, `${SMS_message.OTP_verification} ${otp}`)
            response = `OTP has been sent to ${data?.phoneNumber}`
        }
        await userModel.findOneAndUpdate({ phoneNumber: body?.phoneNumber, isActive: true }, { otp, otpExpireTime: new Date(new Date().setMinutes(new Date().getMinutes() + 10)) })
        return res.status(200).json(new apiResponse(200, `${response}`, {}, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const user_otp_verification = async (req: Request, res: Response) => {
    reqInfo(req)
    let body = req.body, data: any
    try {
        body.isActive = true
        data = await userModel.findOneAndUpdate({ otp: body?.otp }, { $addToSet: { deviceToken: body?.deviceToken }, otp: null, otpExpireTime: null })
        if (!data) return res.status(400).json(new apiResponse(400, responseMessage?.invalidOTP, {}, {}))
        if (data.isBlock == true) return res.status(403).json(new apiResponse(403, responseMessage?.accountBlock, {}, {}))
        if (new Date(data.otpExpireTime).getTime() < new Date().getTime()) return res.status(410).json(new apiResponse(410, responseMessage?.expireOTP, {}, {}))
        const token = jwt.sign({
            _id: data._id,
            authToken: data?.authToken,
            type: data.userType,
            status: "login OTP verification",
            generatedOn: (new Date().getTime())
        }, jwt_token_secret)
        // const refresh_token = jwt.sign({
        //     _id: data._id,
        //     generatedOn: (new Date().getTime())
        // }, refresh_jwt_token_secret)
        if (data) return res.status(200).json(new apiResponse(200, responseMessage?.OTPverified, {
            _id: data._id, authToken: body?.otp,
            phoneNumber: data?.phoneNumber,
            token,
        }, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, 'Internal Server Error', {}, error))
    }
}
export const update_profile = async (req: Request, res: Response) => {
    reqInfo(req)
    let user: any = req.header('user'), emailAlreadyExist: any,
        id = req.body?.id,
        body: any = req.body
    try {
        if (body?.email) {
            emailAlreadyExist = await userModel.findOne({ email: body?.email, _id: { $ne: ObjectId(id) }, isActive: true }, { email: 1 })
            if (emailAlreadyExist) return res.status(409).json(new apiResponse(404, responseMessage?.alreadyEmail, {}, {}))
        }
        let response = await userModel.findOneAndUpdate({ _id: ObjectId(user._id), isActive: true }, body)
        if (body?.profile_image != response?.profile_image && response.profile_image != null && body?.profile_image != null && body?.profile_image != undefined) {
            let [folder_name, image_name] = await URL_decode(response?.profile_image)
            await deleteImage(image_name, folder_name)
        }
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.updateDataSuccess('Your profile'), {}, {}))
        else return res.status(501).json(new apiResponse(501, responseMessage?.updateDataError('Your profile'), {}, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, {}))
    }
}
