import config from 'config';
import mongoose from 'mongoose';
import express from 'express'
// const mongooseConnection = express()
const dbUrl: any = config.get('db_url');

export const mongooseConnection = mongoose.connect(
    dbUrl,
    {
        useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false
    }
).then(result => console.log('Database successfully connected')).catch(err => console.log(err));
