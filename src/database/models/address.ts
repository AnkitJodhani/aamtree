var mongoose = require('mongoose')
const addressSchema = new mongoose.Schema({

    Address: { type: String, default: null },
    State: { type: String, default: null },
    City: { type: String, default: null },
    location: { type: { type: String, default: "Point" }, coordinates: { type: Array, default: [] } },
    isActive: { type: Boolean, default: true, },
    isBlock: { type: Boolean, default: false, },
    image: { type: String, default: null },
    createdBy: { type: mongoose.Schema.Types.ObjectId },
    updatedBy: { type: mongoose.Schema.Types.ObjectId },
}, { timestamps: true })

export const addressModel = mongoose.model('address', addressSchema) 