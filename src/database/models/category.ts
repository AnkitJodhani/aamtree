var mongoose = require('mongoose')
const categorySchema = new mongoose.Schema({
    name: { type: String },
    isActive: { type: Boolean, default: true, },
    isBlock: { type: Boolean, default: false, },
    image: { type: String, default: null },
    createdBy: { type: mongoose.Schema.Types.ObjectId },
    updatedBy: { type: mongoose.Schema.Types.ObjectId },
}, { timestamps: true })

export const categoryModel = mongoose.model('category', categorySchema) 