var mongoose = require('mongoose')
const orderSchema = new mongoose.Schema({
    cartId: { type: [{ type: mongoose.Schema.Types.ObjectId }], default: [] },
    Price: { type: Number, default: 0 },
    Tax: { type: Number, default: 0 },
    FinalPrice: { type: Number, default: 0 },
    status: { type: Number, default: 0, enum: [0, 1, 3] }, //0 = pending  || 1 = completed || 2 = cancelled
    PaymentStatus: { type: Number, default: 0, enum: [0, 1, 2] },//0 = credit card or debit card or net banking || 1 = RBI approved payment method ||2 = cash at the time of delivery
    isActive: { type: Boolean, default: true, },
    isBlock: { type: Boolean, default: false, },
    items: { type: Number, default: false, },
    delivery: { type: Number, default: false, },
    orderTotal: { type: Number, default: false },
    discount: { type: Number, default: false, },
    address: { type: String, default: null, },
    createdBy: { type: mongoose.Schema.Types.ObjectId },
    updatedBy: { type: mongoose.Schema.Types.ObjectId },
    orderType: { type: Number, default: 0, enum: [0, 1] },// 0 -Home  Delivery || 1 - pickup  

}, { timestamps: true })

export const orderModel = mongoose.model('order', orderSchema) 