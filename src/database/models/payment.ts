var mongoose = require('mongoose')
// import mongoose from 'mongoose'
const paymentSchema = new mongoose.Schema({
    isActive: { type: Boolean, default: true },
    createdBy: { type: mongoose.Schema.Types.ObjectId, default: null },
    paymentType: { type: Number, default: 0, enum: [0, 1] },     // 0 - S|| 1 - Post purchase,
    paymentStatus: { type: Number, default: 0, enum: [0, 1, 2] },     // 0 - Pending || 1 - Completed || 2 - Failed
    subscriptionType: { type: String, },
    subscriptionId: { type: String, },
    expireAt: { type: Date, },
    postId: { type: mongoose.Schema.Types.ObjectId },
    price: { type: Number, default: 0 },
}, { timestamps: true })

export const paymentModel = mongoose.model('payment', paymentSchema)