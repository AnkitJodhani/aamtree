var mongoose = require('mongoose')
const productSchema = new mongoose.Schema({
    title: { type: String, default: null },
    isActive: { type: Boolean, default: true, },
    isBlock: { type: Boolean, default: false, },
    price: { type: Number, default: 0 },
    Quantity: { type: Number, default: 0 },
    islimitedQuantity: { type: Boolean, default: true, },
    categoryId: { type: mongoose.Schema.Types.ObjectId },
    image: { type: [{ type: String }], default: [] },
    createdBy: { type: mongoose.Schema.Types.ObjectId },
    updatedBy: { type: mongoose.Schema.Types.ObjectId },
}, { timestamps: true })

export const productModel = mongoose.model('product', productSchema)



