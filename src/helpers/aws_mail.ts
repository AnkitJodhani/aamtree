"use strict"
import config from 'config'
import moment from 'moment-timezone'
import AWS from 'aws-sdk'

const timeZone: any = config.get('timeZone')
const aws: any = config.get("aws")
const logo: any = config.get('logo')
// if (process.env.NODE_ENV == 'development') {
AWS.config.update({
    accessKeyId: "AKIAYSXPRRW3K2GCLJEX",
    secretAccessKey: "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",
    region: "us-east-1"
})
// }
// else {
//     AWS.config.update({
//         accessKeyId: aws.accessKeyId,
//         secretAccessKey: aws.secretAccessKey,
//         region: aws.region
//     })
// }
const ses = new AWS.SES()

export const create_template_mail = async (name: any, email_heading: any, html_code: any) => {
    return new Promise(async (resolve, reject) => {
        let template_data = {
            "Template": {
                "TemplateName": `${name}`,
                "SubjectPart": `${email_heading}`,
                "HtmlPart": `${html_code}`
            }
        }
        ses.createTemplate(template_data, function (err, data) {
            if (err) {
                resolve(err)

                console.log(err, err.stack);
                console.log("------------------------------------------------------------------------------------------------");
            } else {
                console.log(data);
                resolve(`${data}`)
            }
        });
    })
}

export const forgot_password_mail = (user, otp) => {
    return new Promise(async (resolve, reject) => {
        var params = {
            Destination: {
                ToAddresses: [user.email]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data: `<html lang="en-US">
                                <head>
                                    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                                    <title>Reset Password Email Template</title>
                                    <meta name="description" content="Reset Password Email Template.">
                                    <style type="text/css">
                                        a:hover {text-decoration: underline !important;}
                                    </style>
                                </head>
                                <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                                    <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8" style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                                        <tr>
                                            <td>
                                                <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="height:80px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:20px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                                                <tr>
                                                                    <td style="height:40px;">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding:0 35px;">
                                                                        <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">Unicorn UI forgot password</h1>
                                                                        <span style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                                        <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                                            Hi ${user.name}
                                                                            <br>
                                                                            Someone, hopefully you, has requested to reset the password for your
                                                                            Unicorn UI account.
                                                                            <br>
                                                                            OTP will expire in 10 minutes.
                                                                            <br>
                                                                            Verification code: ${otp}
                                                                            <br>
                                                                            <br>
                                                                            Thanks,
                                                                            <br>
                                                                            Unicorn UI
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="height:40px;">&nbsp;</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <tr>
                                                            <td style="height:20px;">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:center;"><strong>www.unicornui.com</strong></p></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height:80px;">&nbsp;</td>
                                                        </tr>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </body>
                            </html>
                                 `
                    }
                },
                Subject: {
                    Charset: "UTF-8",
                    Data: "Forgot password"
                }
            },
            ReplyToAddresses: ["unicornui@unicornui.com"],
            Source: "Unicorn UI<unicornui@unicornui.com>",
        };
        // var params =
        // {
        //     Source: "Unicorn UI<unicornui@unicornui.com>",
        //     Template: "fp",
        //     Destination: { ToAddresses: [user.email] },
        //     TemplateData: `{ \"name\":\"${user.name}\",\"otp\":\"${otp}\" }`
        // }
        // ses.sendTemplatedEmail
        ses.sendEmail(params, function (err, data) {
            if (err) {
                reject(err)
                console.log(err, err.stack); // an error occurred
            }
            else {
                resolve(`Email has been sent to ${user.email}, kindly follow the instructions`)
                console.log(data);
            }      // successful response
        });
    })
}

export const email_verification_mail = (user, otp) => {
    return new Promise(async (resolve, reject) => {
        var params = {
            Destination: {
                ToAddresses: [user.email]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data: `<html lang="en-US">
                        <head>
                            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                            <title>Verification Email Template</title>
                            <meta name="description" content="Reset Password Email Template.">
                            <style type="text/css">
                                a:hover {text-decoration: underline !important;}
                            </style>
                        </head>
                        <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                            <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8" style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                                <tr>
                                    <td>
                                        <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="height:80px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="height:20px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:0 35px;">
                                                                <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">Unicorn UI email verification</h1>
                                                                <span style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                                    Hi ${user.name}
                                                                    <br>
                                                                    Someone, hopefully you, has requested new account in Unicorn UI.
                                                                    <br>
                                                                    Please verify your email using otp code.
                                                                    <br>
                                                                    OTP will expire in 10 minutes.
                                                                    <br>
                                                                    Verification code: ${otp}
                                                                    <br>
                                                                    <br>
                                                                    Thanks,
                                                                    <br>
                                                                    Unicorn UI
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <tr>
                                                    <td style="height:20px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center;"><strong>www.unicornui.com</strong></p></td>
                                                </tr>
                                                <tr>
                                                    <td style="height:80px;">&nbsp;</td>
                                                </tr>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </body>
                    </html>
                         `
                    }
                },
                Subject: {
                    Charset: "UTF-8",
                    Data: "Email Verification"
                }
            },
            ReplyToAddresses: ["unicornui@unicornui.com"],
            Source: "Unicorn UI<unicornui@unicornui.com>",
        };
        // var params =
        // {
        //     Source: "Unicorn UI<unicornui@unicornui.com>",
        //     Template: "fp",
        //     Destination: { ToAddresses: [user.email] },
        //     TemplateData: `{ \"name\":\"${user.name}\",\"otp\":\"${otp}\" }`
        // }
        ses.sendEmail(params, function (err, data) {
            if (err) {
                reject(err)
                console.log(err, err.stack); // an error occurred
            }
            else {
                resolve(`Email has been sent to ${user.email}, kindly follow the instructions`)
                console.log(data);
            }      // successful response
        });
    })
}

export const post_rejecting_mail = async (user: any, message: any) => {
    return new Promise(async (resolve, reject) => {
        var params = {
            Destination: {
                ToAddresses: [user.email]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data: `<html lang="en-US">
                        <head>
                            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                            <title>Post Reject Email Template</title>
                            <meta name="description" content="Reset Password Email Template.">
                            <style type="text/css">
                                a:hover {text-decoration: underline !important;}
                            </style>
                        </head>
                        <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                            <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8" style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                                <tr>
                                    <td>
                                        <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="height:80px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="height:20px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:0 35px;">
                                                                <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">Unicorn UI post rejected</h1>
                                                                <span style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                                    Hi ${user.name}
                                                                    <br>
                                                                    Your post has been rejected By admin,
                                                                    <br>
                                                                    found some irrelevant information regarding the post
                                                                    <br>
                                                                    Please change details in your post, following instruction
                                                                    <br>
                                                                    ${message}
                                                                    <br>
                                                                    <br>
                                                                    Thanks,
                                                                    <br>
                                                                    Unicorn UI
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <tr>
                                                    <td style="height:20px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center;"><strong>www.unicornui.com</strong></p></td>
                                                </tr>
                                                <tr>
                                                    <td style="height:80px;">&nbsp;</td>
                                                </tr>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </body>
                    </html>
                         `
                    }
                },
                Subject: {
                    Charset: "UTF-8",
                    Data: "Action on post"
                }
            },
            ReplyToAddresses: ["unicornui@unicornui.com"],
            Source: "Unicorn UI<unicornui@unicornui.com>",
        };
        // var params =
        // {
        //     Source: "Unicorn UI<unicornui@unicornui.com>",
        //     Template: "fp",
        //     Destination: { ToAddresses: [user.email] },
        //     TemplateData: `{ \"name\":\"${user.name}\",\"otp\":\"${otp}\" }`
        // }
        ses.sendEmail(params, function (err, data) {
            if (err) {
                reject(err)
                console.log(err, err.stack); // an error occurred
            }
            else {
                resolve(`Email has been sent to ${user.email}, kindly follow the instructions`)
                console.log(data);
            }      // successful response
        });
    })
}

export const post_approve_mail = async (user: any) => {
    return new Promise(async (resolve, reject) => {
        var params = {
            Destination: {
                ToAddresses: [user.email]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data: `<html lang="en-US">
                        <head>
                            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                            <title>Post Approve Email Template</title>
                            <meta name="description" content="Reset Password Email Template.">
                            <style type="text/css">
                                a:hover {text-decoration: underline !important;}
                            </style>
                        </head>
                        <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                            <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8" style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                                <tr>
                                    <td>
                                        <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="height:80px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="height:20px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:0 35px;">
                                                                <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">Unicorn UI post approved</h1>
                                                                <span style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                                    Hi ${user.name}
                                                                    <br>
                                                                    Congratulation, Your post has been approved By Unicorn UI Team
                                                                    <br>
                                                                    Now you can see your post in trending section
                                                                    <br>
                                                                    <br>
                                                                    Thanks,
                                                                    <br>
                                                                    Unicorn UI
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <tr>
                                                    <td style="height:20px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center;"><strong>www.unicornui.com</strong></p></td>
                                                </tr>
                                                <tr>
                                                    <td style="height:80px;">&nbsp;</td>
                                                </tr>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </body>
                    </html>
                         `
                    }
                },
                Subject: {
                    Charset: "UTF-8",
                    Data: "Action on post"
                }
            },
            ReplyToAddresses: ["unicornui@unicornui.com"],
            Source: "Unicorn UI<unicornui@unicornui.com>",
        };
        // var params =
        // {
        //     Source: "Unicorn UI<unicornui@unicornui.com>",
        //     Template: "fp",
        //     Destination: { ToAddresses: [user.email] },
        //     TemplateData: `{ \"name\":\"${user.name}\",\"otp\":\"${otp}\" }`
        // }
        ses.sendEmail(params, function (err, data) {
            if (err) {
                reject(err)
                console.log(err, err.stack); // an error occurred
            }
            else {
                resolve(`Email has been sent to ${user.email}, kindly follow the instructions`)
                console.log(data);
            }      // successful response
        });
    })
}

export const user_profile_email_verification = async (user: any) => {
    return new Promise(async (resolve, reject) => {
        var params = {
            Destination: {
                ToAddresses: [user.email]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data: `<html lang="en-US">
                        <head>
                            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                            <title>Profile verification</title>
                            <meta name="description" content="Profile verification">
                            <style type="text/css">
                                a:hover {text-decoration: underline !important;}
                            </style>
                        </head>
                        <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                            <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8" style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                                <tr>
                                    <td>
                                        <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="height:80px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="height:20px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:0 35px;">
                                                                <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">Unicorn UI Profile Verification</h1>
                                                                <span style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                                    Hi ${user.name}
                                                                    <br>
                                                                    Congratulation, Your profile has been verified by UnicornUI Team.
                                                                    <br>
                                                                    Now you are qualified for a premium user.
                                                                    <br>
                                                                    Click the below button for more information on benefits.
                                                                    <br>
                                                                    <br>
                                                                    <a style='padding: 8px 15px; background-color: #13c1e9; list-style: none; color: #fff; text-decoration: none; border-radius: 5px;'
                                                                        href=https://www.unicornui.com/subscription title='logo'
                                                                        target='_blank'>Click Here
                                                                    </a>
                                                                    <br>
                                                                    <br>
                                                                    Thanks,
                                                                    <br>
                                                                    Unicorn UI
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <tr>
                                                    <td style="height:20px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center;"><strong>www.unicornui.com</strong></p></td>
                                                </tr>
                                                <tr>
                                                    <td style="height:80px;">&nbsp;</td>
                                                </tr>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </body>
                    </html>
                         `
                    }
                },
                Subject: {
                    Charset: "UTF-8",
                    Data: "Profile verification"
                }
            },
            ReplyToAddresses: ["unicornui@unicornui.com"],
            Source: "Unicorn UI<unicornui@unicornui.com>",
        };
        // var params =
        // {
        //     Source: "Unicorn UI<unicornui@unicornui.com>",
        //     Template: "fp",
        //     Destination: { ToAddresses: [user.email] },
        //     TemplateData: `{ \"name\":\"${user.name}\",\"otp\":\"${otp}\" }`
        // }
        ses.sendEmail(params, function (err, data) {
            if (err) {
                reject(err)
                console.log(err, err.stack); // an error occurred
            }
            else {
                resolve(`Email has been sent to ${user.email}, kindly follow the instructions`)
                console.log(data);
            }      // successful response
        });
    })
}

export const user_profile_email_unVerification = async (user: any, message: any) => {
    return new Promise(async (resolve, reject) => {
        var params = {
            Destination: {
                ToAddresses: [user.email]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data: `<html lang="en-US">
                        <head>
                            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                            <title>Profile un verification</title>
                            <meta name="description" content="Profile un verification">
                            <style type="text/css">
                                a:hover {text-decoration: underline !important;}
                            </style>
                        </head>
                        <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                            <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8" style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                                <tr>
                                    <td>
                                        <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="height:80px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="height:20px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:0 35px;">
                                                                <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">Unicorn UI Profile Verification</h1>
                                                                <span style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                                    Hi ${user.name}
                                                                    <br>
                                                                    ${message}
                                                                    <br>
                                                                    Unfortunately, Our UnicornUi team decide that your account revoke access to
                                                                    a premium user.
                                                                    <br>
                                                                    <br>
                                                                    Thanks,
                                                                    <br>
                                                                    Unicorn UI
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <tr>
                                                    <td style="height:20px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center;"><strong>www.unicornui.com</strong></p></td>
                                                </tr>
                                                <tr>
                                                    <td style="height:80px;">&nbsp;</td>
                                                </tr>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </body>
                    </html>
                         `
                    }
                },
                Subject: {
                    Charset: "UTF-8",
                    Data: "Profile verification"
                }
            },
            ReplyToAddresses: ["unicornui@unicornui.com"],
            Source: "Unicorn UI<unicornui@unicornui.com>",
        };
        // var params =
        // {
        //     Source: "Unicorn UI<unicornui@unicornui.com>",
        //     Template: "fp",
        //     Destination: { ToAddresses: [user.email] },
        //     TemplateData: `{ \"name\":\"${user.name}\",\"otp\":\"${otp}\" }`
        // }
        ses.sendEmail(params, function (err, data) {
            if (err) {
                reject(err)
                console.log(err, err.stack); // an error occurred
            }
            else {
                resolve(`Email has been sent to ${user.email}, kindly follow the instructions`)
                console.log(data);
            }      // successful response
        });
    })
}

export const report_reply_mail = async (user: any, message: any) => {
    return new Promise(async (resolve, reject) => {
        var params = {
            Destination: {
                ToAddresses: [user.email]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data: `<html lang="en-US">
                        <head>
                            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                            <title>Report reply email</title>
                            <meta name="description" content="Report reply email">
                            <style type="text/css">
                                a:hover {text-decoration: underline !important;}
                            </style>
                        </head>
                        <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                            <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8" style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                                <tr>
                                    <td>
                                        <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="height:80px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:center;">
                                                    <a href="https://www.unicornui.com/" title="logo" target="_blank">
                                                        <img src=${logo} height="130px" title="logo" alt="logo">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height:20px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:0 35px;">
                                                                <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">Unicorn UI Report</h1>
                                                                <span style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                                    Hi ${user.name}
                                                                    <br>
                                                                    ${message}
                                                                    <br>
                                                                    <br>
                                                                    Thanks,
                                                                    <br>
                                                                    Unicorn UI
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <tr>
                                                    <td style="height:20px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center;"><strong>www.unicornui.com</strong></p></td>
                                                </tr>
                                                <tr>
                                                    <td style="height:80px;">&nbsp;</td>
                                                </tr>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </body>
                    </html>
                         `
                    }
                },
                Subject: {
                    Charset: "UTF-8",
                    Data: "Report"
                }
            },
            ReplyToAddresses: ["unicornui@unicornui.com"],
            Source: "Unicorn UI<unicornui@unicornui.com>",
        };
        // var params =
        // {
        //     Source: "Unicorn UI<unicornui@unicornui.com>",
        //     Template: "fp",
        //     Destination: { ToAddresses: [user.email] },
        //     TemplateData: `{ \"name\":\"${user.name}\",\"otp\":\"${otp}\" }`
        // }
        ses.sendEmail(params, function (err, data) {
            if (err) {
                reject(err)
                console.log(err, err.stack); // an error occurred
            }
            else {
                resolve(`Email has been sent to ${user.email}, kindly follow the instructions`)
                console.log(data);
            }      // successful response
        });
    })
}

export const plan_purchase_reply_mail = async (user: any, message: any) => {
    console.log(user)
    return new Promise(async (resolve, reject) => {
        var params = {
            Destination: {
                ToAddresses: [user.email]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data: `<html lang="en-US">
                        <head>
                            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                            <title>Plan purchase email</title>
                            <meta name="description" content="Plan purchase email">
                            <style type="text/css">
                                a:hover {text-decoration: underline !important;}
                            </style>
                        </head>
                        <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                            <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8" style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                                <tr>
                                    <td>
                                        <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="height:80px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:center;">
                                                    <a href="https://www.unicornui.com/" title="logo" target="_blank">
                                                        <img src=${logo} height="130px" title="logo" alt="logo">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height:20px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:0 35px;">
                                                                <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">Unicorn UI Plan Purchase</h1>
                                                                <span style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                                    Hi ${user.name}
                                                                    <br>
                                                                    ${message}
                                                                    <br>
                                                                    <br>
                                                                    Thanks,
                                                                    <br>
                                                                    Unicorn UI
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <tr>
                                                    <td style="height:20px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center;"><strong>www.unicornui.com</strong></p></td>
                                                </tr>
                                                <tr>
                                                    <td style="height:80px;">&nbsp;</td>
                                                </tr>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </body>
                    </html>
                         `
                    }
                },
                Subject: {
                    Charset: "UTF-8",
                    Data: "plan_purchase"
                }
            },
            ReplyToAddresses: ["unicornui@unicornui.com"],
            Source: "Unicorn UI<unicornui@unicornui.com>",
        };
        // var params =
        // {
        //     Source: "Unicorn UI<unicornui@unicornui.com>",
        //     Template: "fp",
        //     Destination: { ToAddresses: [user.email] },
        //     TemplateData: `{ \"name\":\"${user.name}\",\"otp\":\"${otp}\" }`
        // }
        ses.sendEmail(params, function (err, data) {
            if (err) {
                reject(err)
                console.log(err, err.stack); // an error occurred
            }
            else {
                resolve(`Email has been sent to ${user.email}, kindly follow the instructions`)
                console.log(data);
            }      // successful response
        });
    })
}
export const post_update_mail = async (user: any, message: any) => {
    return new Promise(async (resolve, reject) => {
        var params = {
            Destination: {
                ToAddresses: [user.email]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data: `<html lang="en-US">
                        <head>
                            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                            <title>Post update email</title>
                            <meta name="description" content="Post update email">
                            <style type="text/css">
                                a:hover {text-decoration: underline !important;}
                            </style>
                        </head>
                        <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                            <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8" style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                                <tr>
                                    <td>
                                        <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="height:80px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:center;">
                                                    <a href="https://www.unicornui.com/" title="logo" target="_blank">
                                                        <img src=${logo} height="130px" title="logo" alt="logo">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height:20px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:0 35px;">
                                                                <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">
                                                                    Unicorn UI Post Update</h1>
                                                                <span style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                                    Hi ${user.name}
                                                                    <br>
                                                                    ${message}
                                                                    <br>
                                                                    <br>
                                                                    Thanks,
                                                                    <br>
                                                                    Unicorn UI
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <tr>
                                                    <td style="height:20px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center;"><strong>www.unicornui.com</strong></p></td>
                                                </tr>
                                                <tr>
                                                    <td style="height:80px;">&nbsp;</td>
                                                </tr>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </body>
                    </html>
                         `
                    }
                },
                Subject: {
                    Charset: "UTF-8",
                    Data: "Report"
                }
            },
            ReplyToAddresses: ["unicornui@unicornui.com"],
            Source: "Unicorn UI<unicornui@unicornui.com>",
        };
        // var params =
        // {
        //     Source: "Unicorn UI<unicornui@unicornui.com>",
        //     Template: "fp",
        //     Destination: { ToAddresses: [user.email] },
        //     TemplateData: `{ \"name\":\"${user.name}\",\"otp\":\"${otp}\" }`
        // }
        ses.sendEmail(params, function (err, data) {
            if (err) {
                reject(err)
                console.log(err, err.stack); // an error occurred
            }
            else {
                resolve(`Email has been sent to ${user.email}, kindly follow the instructions`)
                console.log(data);
            }      // successful response
        });
    })
}
export const post_delete_mail = async (user: any, message: any) => {
    return new Promise(async (resolve, reject) => {
        var params = {
            Destination: {
                ToAddresses: [user.email]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data: `<html lang="en-US">
                        <head>
                            <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                            <title>Post delete email</title>
                            <meta name="description" content="Post delete email">
                            <style type="text/css">
                                a:hover {text-decoration: underline !important;}
                            </style>
                        </head>
                        <body marginheight="0" topmargin="0" marginwidth="0" style="margin: 0px; background-color: #f2f3f8;" leftmargin="0">
                            <table cellspacing="0" border="0" cellpadding="0" width="100%" bgcolor="#f2f3f8" style="@import url(https://fonts.googleapis.com/css?family=Rubik:300,400,500,700|Open+Sans:300,400,600,700); font-family: 'Open Sans', sans-serif;">
                                <tr>
                                    <td>
                                        <table style="background-color: #f2f3f8; max-width:670px;  margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="height:80px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td style="text-align:center;">
                                                    <a href="https://www.unicornui.com/" title="logo" target="_blank">
                                                        <img src=${logo} height="130px" title="logo" alt="logo">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height:20px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px;background:#fff; border-radius:3px; text-align:center;-webkit-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);-moz-box-shadow:0 6px 18px 0 rgba(0,0,0,.06);box-shadow:0 6px 18px 0 rgba(0,0,0,.06);">
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding:0 35px;">
                                                                <h1 style="color:#1e1e2d; font-weight:500; margin:0;font-size:32px;font-family:'Rubik',sans-serif;">
                                                                    Unicorn UI Post Delete</h1>
                                                                <span style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; width:100px;"></span>
                                                                <p style="color:#455056; font-size:15px;line-height:24px; margin:0;">
                                                                    Hi ${user.name}
                                                                    <br>
                                                                    ${message}
                                                                    <br>
                                                                    <br>
                                                                    Thanks,
                                                                    <br>
                                                                    Unicorn UI
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height:40px;">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <tr>
                                                    <td style="height:20px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:center;"><strong>www.unicornui.com</strong></p></td>
                                                </tr>
                                                <tr>
                                                    <td style="height:80px;">&nbsp;</td>
                                                </tr>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </body>
                    </html>
                         `
                    }
                },
                Subject: {
                    Charset: "UTF-8",
                    Data: "Report"
                }
            },
            ReplyToAddresses: ["unicornui@unicornui.com"],
            Source: "Unicorn UI<unicornui@unicornui.com>",
        };
        // var params =
        // {
        //     Source: "Unicorn UI<unicornui@unicornui.com>",
        //     Template: "fp",
        //     Destination: { ToAddresses: [user.email] },
        //     TemplateData: `{ \"name\":\"${user.name}\",\"otp\":\"${otp}\" }`
        // }
        ses.sendEmail(params, function (err, data) {
            if (err) {
                reject(err)
                console.log(err, err.stack); // an error occurred
            }
            else {
                resolve(`Email has been sent to ${user.email}, kindly follow the instructions`)
                console.log(data);
            }      // successful response
        });
    })
}

export const campaign_template_mail = async (emails: any, name: any) => {
    return new Promise(async (resolve, reject) => {
        var params =
        {
            Source: "Unicorn UI<unicornui@unicornui.com>",
            Template: `${name}`,
            Destinations: emails,
            DefaultTemplateData: "{ \"name\":\"friend\"}"
        }
        ses.sendBulkTemplatedEmail(params, function (err, data) {
            if (err) {
                reject(err)
                console.log(err);
            }
            else {
                resolve(`Email has been sent to all users, kindly follow the instructions`)
                console.log(data);
            }
        });
    })
}