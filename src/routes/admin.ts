"use strict"
import { Router } from 'express'
import { adminController, } from '../controllers'
import { userJWT } from '../helpers/jwt'
import * as validation from '../validation'
import { add_product } from '../validation'


const router = Router()

//  ------   User Routes   ------  
router.post('/sign', validation?.admin_signUp, adminController.admin_signUp)
router.post('/login', validation?.admin_login, adminController.admin_login)
router.post('/forget_password', validation?.forgot_password, adminController.forgot_password)
router.post('/reset_password/', validation?.reset_password, adminController.reset_password)
router.post('/admin_otp_verification', validation?.admin_otp_verification, adminController.admin_otp_verification)


router.use(userJWT)

router.put('/profile/update', validation.update_profile, adminController.update_profile)

//------------- category ----------------//
router.post('/category', validation?.add_category, adminController.add_category)
router.post('/category/get', adminController.get_category_pagination)
router.put('/category/update', validation?.update_category, adminController.update_category)
router.delete('/delete/:id', adminController.delete_category)
router.get('/by_id/:id', adminController.by_id_category)

//--------- shop Routes ------------
router.post('/shop/add', validation?.add_shop, adminController.add_shop)
router.post('/shop/get', adminController.get_shop)
router.put('/shop/update', validation?.update_shop, adminController.update_shop)
router.delete('/shop/:id', adminController.delete_shop)

//--------- shop Product ------------
router.post('/product/get', adminController.get_product_pagination)
router.post('/product/add', validation?.add_product, adminController.add_product)
router.put('/product/update', validation?.update_product, adminController.update_product)
router.delete('/product/:id', adminController.delete_product)
router.get('/product/by_id/:id', adminController.by_id_product)

//--------- user Routes ------------
router.post('/product/get', adminController.get_product_pagination)

//---------- user Router ------------
router.post('/update_profile', adminController.update_profile)
router.get('/get_profile', adminController.get_profile)




export const adminRouter = router
