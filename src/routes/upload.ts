"use strict"
import { Router } from 'express'
import { file_upload_response, uploadS3, userJWT } from '../helpers'
import * as validation from '../validation/index'

const router = Router()

router.use(userJWT)
router.post('/image/:file', validation?.file_type, uploadS3.single('Janki'), file_upload_response)

export const uploadRouter = router