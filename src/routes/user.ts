"use strict"
import { Router } from 'express'
import { userController } from '../controllers'
import { userJWT } from '../helpers/jwt'
import * as validation from '../validation'

const router = Router()

//  ------   User Routes   ------  

router.post('/sign', validation?.signUp, userController.signUp)
router.post('/login', validation?.login, userController.login)
router.post('/otp_verification', validation?.user_otp_verification, userController.user_otp_verification)

router.use(userJWT)



//--------- category Routes ------------
router.post('/category/get', userController.get_category_pagination)

//--------- shop Routes ------------
// router.post('/shop/add', userController.add_order_shop)
router.post('/get', userController.get_shop)

//--------- product Routes ------------
router.post('/product/get', userController.get_product_pagination)

//--------- address Routes ------------
router.post('/address/add', validation.add_address, userController.add_address)
router.put('/address/update', validation.update_address, userController.update_address)
router.get('/address/get', userController.get_address)
router.get('/address/:id', userController.by_id_address)
router.delete('/delete/:id', userController.delete_address)

//--------- cart Routes ------------
router.post('/cart/add', validation.add_cart, userController.add_cart)
router.get('/cart/:id', userController.increment_cart_quantity)
router.get('/cart/get/:id', userController.decrement_cart_quantity)
router.post('/cart/pagination', userController.get_cart_pagination)

//---------- order Routes -----------
router.post('/order', userController.get_order_pagination)
router.post('/order/add', validation.add_order, userController.add_order)
router.put('/order/update', validation.update_order, userController.update_order)
router.delete('/order/:id', userController.delete_order)
router.get('/order/by_id/:id', validation.by_id, userController.by_id_order)
router.post('/order/user/:id', userController.order_by_user)
router.post('/order/get_order', userController.get_order_status)
router.post('/order/cancel/:id', userController.cancel_order)

//--------- tracker order ----------
router.post('/tracker/order', validation.add_tracker_order, userController.add_tracker_order)

export const userRouter = router