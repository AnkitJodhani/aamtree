"use strict"
import * as Joi from "joi"
import { apiResponse } from '../common'
import { isValidObjectId } from 'mongoose'
import { Request, Response } from 'express'

export const add_address = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        Address: Joi.string().required().error(new Error('address is required!')),
        state: Joi.string().required().error(new Error('state is required!')),
        city: Joi.string().required().error(new Error('city is required!')),
        Pincode: Joi.number().required().error(new Error('Pincode is required!')),
        Price: Joi.number().required().error(new Error('Price is required!')),
        location: Joi.object().required().error(new Error('location is required!')),
        countryCode: Joi.string().required().error(new Error('countryCode is required!')),
    })
    schema.validateAsync(req.body).then(result => {
        return next()
    }).catch(error => { res.status(400).json(new apiResponse(400, error.message, {}, {})) })
}
export const update_address = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        id: Joi.string().required().error(new Error('id is required!')),
        Address: Joi.string().required().error(new Error('address is required!')),
        state: Joi.string().required().error(new Error('state is required!')),
        city: Joi.string().required().error(new Error('city is required!')),
        Pincode: Joi.number().required().error(new Error('Pincode is required!')),
        Price: Joi.number().required().error(new Error('Price is required!')),
        location: Joi.object().required().error(new Error('location is required!')),
        countryCode: Joi.string().required().error(new Error('countryCode is required!')),
    })
    schema.validateAsync(req.body).then(async result => {
        if (!isValidObjectId(result.id)) return res.status(400).json(new apiResponse(400, "Invalid id format", {}, {}));
        return next()
    }).catch(async error => { res.status(400).json(new apiResponse(400, error.message, {}, {})) })
}
