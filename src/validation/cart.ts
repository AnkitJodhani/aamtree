"use strict"
import * as Joi from "joi"
import { apiResponse } from '../common'
import { isValidObjectId } from 'mongoose'
import { Request, Response } from 'express'

export const add_cart = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        quantity: Joi.number().required().error(new Error('Pincode is required!')),
        price: Joi.number().required().error(new Error('Price is required!')),
        productId: Joi.string().required().trim().error(new Error('productId is required!')),
        cartTotal: Joi.number().required().error(new Error('cartTotal is required!')),
        delivery: Joi.number().required().error(new Error('delivery is required!')),
        discount: Joi.number().required().error(new Error('discount is required!')),
        orderTotal: Joi.number().required().error(new Error('orderTotal is required!')),
    })
    schema.validateAsync(req.body).then(result => {
        return next()
    }).catch(error => { res.status(400).json(new apiResponse(400, error.message, {}, {})) })
}