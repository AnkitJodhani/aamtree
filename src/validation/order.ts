"use strict"
import * as Joi from "joi"
import { apiResponse } from '../common'
import { isValidObjectId } from 'mongoose'
import { Request, Response } from 'express'

export const add_order = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        Tax: Joi.number().required().error(new Error('Pincode is required!')),
        FinalPrice: Joi.number().required().error(new Error('FinalPrice is required!')),
        status: Joi.number().required().error(new Error('status is required!')),
        PaymentStatus: Joi.number().required().error(new Error('PaymentStatus is required!')),
        Price: Joi.number().required().error(new Error('Price is required!')),
        location: Joi.object().required().error(new Error('location is required!')),
        cartId: Joi.array().required().error(new Error('cartId is required!')),
        orderType: Joi.number().error(new Error('orderType is number')),
        address: Joi.string().required().error(new Error('address is required!')),
        items: Joi.number().required().error(new Error('items is required!')),
        delivery: Joi.number().required().error(new Error('delivery is required!')),
        orderTotal: Joi.number().required().error(new Error('orderTotal is required!')),

    })
    schema.validateAsync(req.body).then(result => {
        return next()
    }).catch(error => { res.status(400).json(new apiResponse(400, error.message, {}, {})) })
}
export const update_order = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        id: Joi.string().required().error(new Error('id is required!')),
        Tax: Joi.number().required().error(new Error('Pincode is required!')),
        FinalPrice: Joi.number().required().error(new Error('FinalPrice is required!')),
        status: Joi.number().required().error(new Error('status is required!')),
        PaymentStatus: Joi.number().required().error(new Error('PaymentStatus is required!')),
        Price: Joi.number().required().error(new Error('Price is required!')),
        location: Joi.object().required().error(new Error('location is required!')),
        cartId: Joi.array().required().error(new Error('cartId is required!')),
        orderType: Joi.number().error(new Error('orderType is number')),
        address: Joi.string().required().error(new Error('address is required!')),
        items: Joi.number().required().error(new Error('items is required!')),
        delivery: Joi.number().required().error(new Error('delivery is required!')),
        orderTotal: Joi.number().required().error(new Error('orderTotal is required!')),

    })
    schema.validateAsync(req.body).then(result => {
        if (!isValidObjectId(result.id)) return res.status(400).json(new apiResponse(400, "Invalid id format", {}, {}));
        return next()
    }).catch(error => { res.status(400).json(new apiResponse(400, error.message, {}, {})) })
}
export const by_id = async (req: Request, res: Response, next: any) => {
    if (!isValidObjectId(req.params.id)) return res.status(400).json(new apiResponse(400, 'invalid id', {}, {}))
    return next()
}
