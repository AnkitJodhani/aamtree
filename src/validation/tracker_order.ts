"use strict"
import * as Joi from "joi"
import { apiResponse } from '../common'
import { isValidObjectId } from 'mongoose'
import { Request, Response } from 'express'

export const add_tracker_order = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        estimated_Date: Joi.string().required().error(new Error('estimated_Date is required!')),
        pickup_Date: Joi.string().required().error(new Error('pickup_date is required ')),

    })
    schema.validateAsync(req.body).then(async result => {
        return next()
    }).catch(async error => { res.status(400).json(new apiResponse(400, error.message, {}, {})) })
}